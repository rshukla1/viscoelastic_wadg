#define NGEO   (p_Nvgeo+p_Nfgeo*p_Nfaces) // total number of geometric factors
#define ddot4(a,b)  a.x*b.x + a.y*b.y + a.z*b.z + a.w*b.w

// defined in WaveOKL3d (initWave3d)
#if USE_DOUBLE
#define dfloat double
#define dfloat4 double4
#else
#define dfloat float
#define dfloat4 float4
#endif


//  =============== RK first order DG kernels ===============
#define rx sG[k2][0]
#define ry sG[k2][1]
#define rz sG[k2][2]
#define sx sG[k2][3]
#define sy sG[k2][4]
#define sz sG[k2][5]
#define tx sG[k2][6]
#define ty sG[k2][7]
#define tz sG[k2][8]

kernel void rk_volume_elas(const    int K,
			   const dfloat * restrict vgeo,
			   const dfloat * restrict Dr,
			   const dfloat * restrict Ds,
			   const dfloat * restrict Dt,
			   const dfloat * restrict Q,
			   dfloat * restrict rhsQ){

  // loop over elements
  for(int k1=0; k1<(K+p_KblkV-1)/p_KblkV; ++k1; outer0){

    // total shared memory amounts to approx. 4 dfloats per thread
    shared dfloat sQ[p_KblkV][p_Nfields][p_Np];
    shared dfloat sG[p_KblkV][p_Nvgeo];

    // loop has to survive multiple inner loops
    for(int k2 = 0; k2 < p_KblkV; ++k2; inner1){
      for(int i=0;i<p_Np;++i;inner0){
	int k = k1*p_KblkV + k2;
	if (k < K){

          // load geometric factors into shared memory
          int m = i;
          while(m<p_Nvgeo){
            sG[k2][m] = vgeo[m + p_Nvgeo*k];
           m += p_Np;
          }

             
          // load Q into shared memory for element k
	  int offset = 0;
	  const int id = i + k*p_Np*p_Nfields;
	  for (int fld = 0; fld < p_Nfields; ++fld){
	    sQ[k2][fld][i] = Q[id + offset];
	    offset += p_Np;
	  }

        }
      }
    }
    barrier(localMemFence);

    for(int k2 = 0; k2 < p_KblkV; ++k2; inner1){
      // loop over nodes
      for(int i=0;i<p_Np;++i;inner0){
	int k = k1*p_KblkV + k2;
	if (k < K){

	  dfloat Qr[p_Nfields], Qs[p_Nfields], Qt[p_Nfields];
	  for (int fld = 0; fld < p_Nfields; ++fld){
	    Qr[fld] = 0.f; Qs[fld] = 0.f; Qt[fld] = 0.f;
	  }

	  for(int j=0;j<p_Np;++j){
	    const dfloat Dr_ij = Dr[i+j*p_Np];
            //if(k==0){printf("Dr is %f\n", Dr_ij);}
	    const dfloat Ds_ij = Ds[i+j*p_Np];
	    const dfloat Dt_ij = Dt[i+j*p_Np];

	    for (int fld = 0; fld < p_Nfields; ++fld){
	      const dfloat Qfld = sQ[k2][fld][j];
	      Qr[fld] += Dr_ij*Qfld;
	      Qs[fld] += Ds_ij*Qfld;
	      Qt[fld] += Dt_ij*Qfld;
	    }
          }
	  dfloat Qx[p_Nfields],Qy[p_Nfields],Qz[p_Nfields];
#if 0
          if(k==0){
               dfloat rxtmp = rx;
               printf("rx is %f\n", rxtmp);

}
#endif
	  for (int fld = 0; fld < p_Nfields; ++fld){
            Qx[fld] = rx*Qr[fld] + sx*Qs[fld] + tx*Qt[fld];
	        Qy[fld] = ry*Qr[fld] + sy*Qs[fld] + ty*Qt[fld];
	        Qz[fld] = rz*Qr[fld] + sz*Qs[fld] + tz*Qt[fld];
	  }

      //   const dfloat divSx = Qx[3] + Qy[8] + Qz[7];
      //   const dfloat divSy = Qx[8] + Qy[4] + Qz[6];
      //   const dfloat divSz = Qx[7] + Qy[6] + Qz[5];

          int id = i + k*p_Nfields*p_Np;
     //   Stress Variables 
          rhsQ[id] =  Qx[12] ;        	            id += p_Np;
          rhsQ[id] =  Qy[13];         	            id += p_Np;
          rhsQ[id] =  Qz[14];        	            id += p_Np;
          rhsQ[id] =  Qy[14] + Qz[13];              id += p_Np;
          rhsQ[id] =  Qx[14] + Qz[12];              id += p_Np;
          rhsQ[id] =  Qx[13] + Qy[12];              id += p_Np;
    //    Memory variables
	      rhsQ[id] =   Qx[12] + Qy[13] + Qz[14];    id += p_Np;
          rhsQ[id] = 2*Qx[12] - Qy[13] - Qz[14] ;   id += p_Np;
          rhsQ[id] = -1*Qx[12] + 2*Qy[13] - Qz[14]; id += p_Np;
          rhsQ[id] =  Qy[14] + Qz[13];              id += p_Np;
          rhsQ[id] =  Qx[14] + Qz[12];              id += p_Np;
          rhsQ[id] =  Qx[13] + Qy[12];              id += p_Np;

//   Memory variables  
          rhsQ[id] =  Qx[0] + Qy[5] + Qz[4];        id += p_Np;
          rhsQ[id] =  Qx[5] + Qy[1] + Qz[3];        id += p_Np;
          rhsQ[id]=   Qx[4] + Qy[3] + Qz[2];
#if 0
	  if (k==0){
	    //dfloat rhsx1 = Qx[0];
	    //dfloat rhsx2 = Qx[1];
	    //dfloat rhsx3 = Qx[2];
	    //dfloat rhsy1 = Qy[0];
	    //dfloat rhsy2 = Qy[1];
	    dfloat rhsy3 = Qy[2];
	    //dfloat rhsz1 = Qz[0];
	    //dfloat rhsz2 = Qz[1];
	    //dfloat rhsz3 = Qz[2];
	    printf("vol terms[%d] = %g, %g, %g, %g, %g, %g, %g, %g, %g\n",
		   i,rhsy3);//,rhsx2,rhsx3,
		   //rhsy1,rhsy2,rhsy3,
		   //rhsz1,rhsz2,rhsz3);
	  }
#endif
        }
      }
    }

  }
}


// split part of kernel
kernel void rk_surface_elas(const    int K,
			    const dfloat * restrict fgeo,
			    const    int * restrict Fmask,
			    const    int * restrict vmapP,
			    const dfloat * restrict LIFT,
			    const dfloat * restrict Q,
			    dfloat * restrict rhsQ){

  // loop over elements
  //printf("In surface kernel");
  for(int k1=0;k1<(K+p_KblkS-1)/p_KblkS;++k1;outer0){

    // total shared memory amounts to approx. 4 dfloats per thread
    shared dfloat s_flux[p_KblkS][p_Nfields][p_NfpNfaces];
    shared dfloat s_nxyz[p_KblkS][3*p_Nfaces];
    for(int k2 = 0; k2 < p_KblkS; ++k2; inner1){
      for(int i=0; i < p_T;++i;inner0){
	int k = k1*p_KblkS + k2;

	if (k < K){
          // retrieve traces (should move earlier)
          if(i<p_NfpNfaces){

            const int f = i/p_Nfp;

	        const int fid = Fmask[i];
            int idM = fid + k*p_Np*p_Nfields;
            int idP = vmapP[i + k*p_NfpNfaces];
	        const int isBoundary = idM==idP;

            int id = f*p_Nfgeo + p_Nfgeo*p_Nfaces*k;
	        const dfloat Fscale = fgeo[id];
	        const dfloat nx = fgeo[id+1];
	        const dfloat ny = fgeo[id+2];
	        const dfloat nz = fgeo[id+3];
	        int foff = 3*f;
	        s_nxyz[k2][foff] = nx; foff++;
	        s_nxyz[k2][foff] = ny; foff++;
	        s_nxyz[k2][foff] = nz;
            
        dfloat dQ[p_Nfields];
	    for (int fld = 0; fld < p_Nfields; ++fld){
	      dQ[fld] = -Q[idM];
	      if (isBoundary==0){ // if interior face. else, QP = 0 for ABC
		     dQ[fld] += Q[idP];
	      }
             else{
               dQ[fld] += -Q[idM]; 
          }

	      idM += p_Np;
	      idP += p_Np;
	    }

	    // central flux terms
	    dfloat fc[p_Nfields];
	    // Stress variables
        fc[0] =   dQ[12]*nx;
	    fc[1] =   dQ[13]*ny;
	    fc[2] =   dQ[14]*nz;
	    fc[3] =   dQ[14]*ny +  dQ[13]*nz;
	    fc[4] =   dQ[14]*nx +  dQ[12]*nz; 
	    fc[5] =   dQ[13]*nx +  dQ[12]*ny;
            // Memory variables
	    fc[6] =   dQ[12]*nx + dQ[13]*ny + dQ[14]*nz;
        fc[7] =   2*dQ[12]*nx - dQ[13]*ny -dQ[14]*nz;
        fc[8] =  -1*dQ[12]*nx + 2*dQ[13]*ny -dQ[14]*nz;
        fc[9] =  dQ[14]*ny + dQ[13]*nz;
        fc[10] = dQ[14]*nx + dQ[12]*nz;
        fc[11] = dQ[13]*nx + dQ[12]*ny;
    
            //Velocity varaibles
	    fc[12] =  dQ[0]*nx  +  dQ[5]*ny  +  dQ[4]*nz; 
	    fc[13] =  dQ[5]*nx  +  dQ[1]*ny  +  dQ[3]*nz;
	    fc[14] =  dQ[4]*nx  +  dQ[3]*ny  +  dQ[2]*nz; 

#if 0
	    if (k==0){
	      dfloat f1 = fc[0];
	      dfloat f2 = fc[1];
	      dfloat f3 = fc[2];
	      dfloat f4 = fc[3];
	      dfloat f5 = fc[4];
	      dfloat f6 = fc[5];
	      dfloat f7 = fc[6];
	      dfloat f8 = fc[7];
	      dfloat f9 = fc[8];
	      dfloat f10 = fc[9];
	      dfloat f11 = fc[10];
	      printf("fc surf_fluxes(%d) = %g, %g, %g, %g, %g, %g\n",
		     i,f1,f2,f3,f4,f5,f6,f7,f8,f9,f10,f11);
	    }
#endif

  	    // Stress penalties
	    
        dfloat fp = fc[12]*nx ;
	    s_flux[k2][0][i] = fc[0] + p_tau_s * fp;

	    fp =fc[13]*ny;
	    s_flux[k2][1][i] = fc[1] + p_tau_s*fp;

	    fp = fc[14]*nz;
	    s_flux[k2][2][i] = fc[2] + p_tau_s*fp;


	    fp = fc[13]*nz + fc[14]*ny;
	    s_flux[k2][3][i] = fc[3] + p_tau_s*fp;

	    fp = fc[12]*nz + fc[14]*nx;
	    s_flux[k2][4][i] = fc[4] + p_tau_s*fp;

	    fp =  fc[12]*ny + fc[13]*nx;
	    s_flux[k2][5][i] = fc[5] + p_tau_s*fp;
//         Memory variables

	    s_flux[k2][6][i] = fc[6];
	   
	    s_flux[k2][7][i] = fc[7];

	    s_flux[k2][8][i] = fc[8];

        s_flux[k2][9][i] = fc[9] ;

        s_flux[k2][10][i] = fc[10];

        s_flux[k2][11][i] = fc[11] ;

//	   Velocity variable

        fp= nx*fc[0] + nz*fc[4] + ny*fc[5] + nx*fc[6] + 2*nx*fc[7] + nz*fc[10] + ny*fc[11];
        s_flux[k2][12][i] = fc[12] + p_tau_s*fp;
	    
        fp= ny*fc[1] + nz*fc[3] + nx*fc[5] + ny*fc[6] - ny*fc[7] + 2*ny*fc[8] - nx*fc[9] + nx*fc[11];
        s_flux[k2][13][i] = fc[13] + p_tau_s*fp;

        fp= nz*fc[2]  + ny*fc[3] + nx*fc[4] + nz*fc[6] - nz*fc[7] + ny*fc[9] + nx*fc[10];
        s_flux[k2][14][i] = fc[14] + p_tau_s*fp;



	    // scale by .5 and J^f/J
	    for (int fld = 0; fld < p_Nfields; ++fld){
	      s_flux[k2][fld][i] *= .5f*Fscale;
	    }
#if 0
	    if (k==0){
	      dfloat f1 = s_flux[k2][0][i];
	      dfloat f2 = s_flux[k2][1][i];
	      dfloat f3 = s_flux[k2][2][i];
	      dfloat f4 = s_flux[k2][3][i];
	      dfloat f5 = s_flux[k2][4][i];
	      dfloat f6 = s_flux[k2][5][i];
	      printf("surf_fluxes(%d) = %g, %g, %g, %g, %g, %g\n",
		     i,f1,f2,f3,f4,f5,f6);
	    }
#endif
          }
        }
      }
    }
    barrier(localMemFence);

    for(int k2 = 0; k2 < p_KblkS; ++k2; inner1){
      for(int i=0; i<p_T; ++i; inner0){

	int k = k1*p_KblkS + k2;
	if (k < K && i<p_Np){

	  // accumulate lift/normal lift contributions
	  int id = i + k*p_Nfields*p_Np;
	  dfloat val[p_Nfields];
	  for (int fld = 0; fld < p_Nfields; ++fld){
	    //val[fld] = rhsQ[id]; id += p_Np;
	    val[fld] = 0.f;
	  }

	  for(int j=0;j<p_NfpNfaces;++j){
	    const dfloat Lij = LIFT[i + j*p_Np];

	    // v testing
	    for (int fld = 0; fld < p_Nfields; ++fld){
	      val[fld] += Lij * s_flux[k2][fld][j];
	    }
	  }
#if 0
	  if (k==0){
	    dfloat rhs1 = val[0];
	    dfloat rhs2 = val[1];
	    dfloat rhs3 = val[2];
	    dfloat rhs4 = val[3];
	    dfloat rhs5 = val[4];
	    dfloat rhs6 = val[5];
	    printf("surf rhs[%d] = %g, %g, %g, %g, %g, %g\n",
		   i,rhs1,rhs2,rhs3,rhs4,rhs5,rhs6);
	  }
#endif

	  id = i + k*p_Nfields*p_Np;
	  for (int fld = 0; fld < p_Nfields; ++fld){
	    rhsQ[id] += val[fld]; id += p_Np;
	  }
        }
      }
    }
  }
}


kernel void rk_update_elas(const int K, const dfloat * restrict Vq, const dfloat * restrict Pq,
                              const dfloat * restrict rhoq, const dfloat * restrict c11q, const dfloat * restrict c12q,
                              const dfloat * restrict c13q, const dfloat * restrict c22q, const dfloat * restrict c23q,
                              const dfloat * restrict c33q, const dfloat * restrict c44q, const dfloat * restrict c55q,
                              const dfloat * restrict c66q, 
                              const dfloat * restrict taus1q, const dfloat * taue1q,
                              const dfloat * restrict taus2q, const dfloat * restrict taue2q, 
                              const dfloat * restrict taus3q, const dfloat * restrict taue3q, 
                              const dfloat * restrict taus4q, const dfloat * restrict taue4q,
                              const dfloat ftime, const dfloat * restrict fsrc, const dfloat fa,
                              const dfloat fb, const dfloat fdt, const dfloat * restrict rhsQ,
                              dfloat * restrict resQ, dfloat * restrict Q){
  //printf("\n I am in update kernel + 11111983\n");
  for(int k1=0; k1<(K+p_KblkU-1)/p_KblkU; ++k1; outer0){
    shared dfloat sQ[p_KblkV][p_Nfields][p_Nq_reduced];
    shared dfloat sD[p_KblkV][p_Nfields][p_Nq_reduced];//Q^-1D matrix

    exclusive dfloat rsxx,rsyy,rszz,rsyz,rsxz,rsxy;
    exclusive dfloat re1, re2, re3, re4, re5, re6, rv1, rv2, rv3;
    exclusive dfloat e1, e2, e3, e4, e5, e6;
    exclusive int k;
 for(int k2 = 0; k2 < p_KblkU; ++k2; inner1){
      for(int i = 0; i < p_Nq_reduced; ++i; inner0){

	k = k1*p_KblkU + k2; // no need for klist here for heterogeneous WADG

	if (k < K && i < p_Np){

	  int id = i + k*p_Np*p_Nfields;
	  for (int fld = 0; fld < p_Nfields; ++fld){
	    sQ[k2][fld][i] = rhsQ[id];
	    sD[k2][fld][i] = Q[id];
	    id += p_Np;
	  }
	  
	}

      }
    }
    barrier(localMemFence);
    for(int k2 = 0; k2 < p_KblkU; ++k2; inner1){
      for(int i = 0; i < p_Nq_reduced; ++i; inner0){

	if (k < K){

	  // interp to quad nodes
	  rsxx = 0.f; rsyy = 0.f; rszz = 0.f;
	  rsyz = 0.f; rsxy = 0.f; rsxz = 0.f;
          
      re1 = 0.f;    re2 = 0.f;  re3 = 0.f;
	  re4 = 0.f;    re5 = 0.f;  re6 = 0.f;
      rv1 = 0.f;    rv2 = 0.f;  rv3 = 0.f;
     
      e1 = 0.f;    e3 = 0.f;  e6 = 0.f;
	  e2 = 0.f;    e4 = 0.f;  e5 = 0.f;
 

         
          //rd1=0.f;rd2=0.f;rd3=0.f;
	  for (int j = 0; j < p_Np; ++j){

	    const dfloat Vq_ij = Vq[i + j*p_Nq_reduced];
	    rsxx += Vq_ij * sQ[k2][0][j];
	    rsyy += Vq_ij * sQ[k2][1][j];
	    rszz += Vq_ij * sQ[k2][2][j];
	    rsyz += Vq_ij * sQ[k2][3][j];
	    rsxz += Vq_ij * sQ[k2][4][j];
	    rsxy += Vq_ij * sQ[k2][5][j];
	    re1  += Vq_ij * sQ[k2][6][j];
	    re2  += Vq_ij * sQ[k2][7][j];
        re3  += Vq_ij * sQ[k2][8][j];
  	    re4  += Vq_ij * sQ[k2][9][j];
	    re5  += Vq_ij * sQ[k2][10][j];
        re6  += Vq_ij * sQ[k2][11][j];
        rv1  += Vq_ij * sQ[k2][12][j];
	    rv2  += Vq_ij * sQ[k2][13][j];
	    rv3  += Vq_ij * sQ[k2][14][j];
        e1   += Vq_ij * sD[k2][6][j];
        e2   += Vq_ij * sD[k2][7][j];
        e3   += Vq_ij * sD[k2][8][j];
        e4   += Vq_ij * sD[k2][9][j];
        e5   += Vq_ij * sD[k2][10][j];
        e6   += Vq_ij * sD[k2][11][j]; 
	  }
	}
      }
    }
    barrier(localMemFence);

    // pointwise scaling
    for(int k2 = 0; k2 < p_KblkU; ++k2; inner1){
      for(int i = 0; i < p_Nq_reduced; ++i; inner0){

	if (k < K){
	  const int id = i + k*p_Nq_reduced;
	  const dfloat rho = rhoq[id];
	  const dfloat c11 = c11q[id];
	  const dfloat c12 = c12q[id];
	  const dfloat c13 = c13q[id];
	  const dfloat c22 = c22q[id];
      const dfloat c23 = c23q[id];
      const dfloat c33 = c33q[id];
	  const dfloat c44 = c44q[id];
      const dfloat c55 = c55q[id];
      const dfloat c66 = c66q[id];
      const dfloat taus1 = taus1q[id];
      const dfloat taue1 = taue1q[id];
      const dfloat taus2 = taus2q[id];
      const dfloat taue2 = taue2q[id];
      const dfloat taus3 = taus3q[id];
      const dfloat taue3 = taue3q[id];
      const dfloat taus4 = taus4q[id];
      const dfloat taue4 = taue4q[id];

	  //Relaxation term
      const dfloat t1 = (1.0/taus1)*((taus1/taue1)-1.0); 
 	  const dfloat t2 = (1.0/taus2)*((taus2/taue2)-1.0); 
      const dfloat t3 = (1.0/taus3)*((taus3/taue3)-1.0); 
 	  const dfloat t4 = (1.0/taus4)*((taus4/taue4)-1.0); 
         
      const dfloat dmat = (1/3)*(c11 + c22 + c33);
      const dfloat gmat = (1/3)*(c44 + c55 + c66);
      const dfloat kmat = dmat - (4/3)*gmat;
          
          // Add diffusive term 
	  sQ[k2][0][i]=c11*rsxx + c12*rsyy + c13*rszz + kmat*e1 + 2*gmat*e2  ;
 	  sQ[k2][1][i]=c12*rsxx + c22*rsyy + c23*rszz + kmat*e1 + 2*gmat*e3 ;
 	  sQ[k2][2][i]=c13*rsxx + c23*rsyy + c33*rszz + kmat*e1 -2*gmat*e2 - 2*gmat*e3 ;
 	  sQ[k2][3][i]=c44*rsyz + c44*e4;
	  sQ[k2][4][i]=c55*rsxz + c55*e5;
	  sQ[k2][5][i]=c66*rsxy + c66*e6;

         //auxillairy variables trms 
	  sQ[k2][6][i]  =  t1*re1 - e1/taus1  ;
	  sQ[k2][7][i]  =  (1.0/3.0)*t2*re2 - e2/taus2 ;
	  sQ[k2][8][i]  =  (1.0/3.0)*t3*re3 - e3/taus3  ;
      sQ[k2][9][i]  =  t2*re4 - e4/taus2 ;	 
	  sQ[k2][10][i] =  t3*re5 - e5/taus3 ;
      sQ[k2][11][i] =  t4*re6 - e6/taus4 ;
         //Fluid Particle Velocity         
	  sQ[k2][12][i] = (1/rho)*rv1;
	  sQ[k2][13][i] = (1/rho)*rv2;
	  sQ[k2][14][i] = (1/rho)*rv3;
	}
      } // inner0
    } // inner1
    barrier(localMemFence);
    // reduce down and increment
    for(int k2 = 0; k2 < p_KblkU; ++k2; inner1){
      for(int i = 0; i < p_Nq_reduced; ++i; inner0){

	if (k < K && i < p_Np){

	  dfloat rQ[p_Nfields];
	  for (int fld = 0; fld < p_Nfields; ++fld){
	    rQ[fld] = 0.f;
	  }
	  for (int j = 0; j < p_Nq_reduced; ++j){
	    const dfloat Pq_ij = Pq[i + j*p_Np];
	    for (int fld = 0; fld < p_Nfields; ++fld){
	      rQ[fld] += Pq_ij * sQ[k2][fld][j];
	    }
	  }

	  // apply ricker pulse to x,y coordinates
        const dfloat ff=fsrc[i + k*p_Np];
	    rQ[2] += ftime*ff;
           //rQ[6] +=(-1)*ftime*ff;


	  int id = i + k*p_Np*p_Nfields;
	  for (int fld = 0; fld < p_Nfields; ++fld){
	    dfloat res = resQ[id];
	    res = fa*res + fdt*rQ[fld];
	    resQ[id] = res;
	    Q[id] += fb*res;
#if 0
	    if (k==0){
	      printf("rk_update: Q[%d] = %f\n", fld,Q[id]);
	    }
#endif
	    id += p_Np;
	  }
	}

      } // inner 0
    } // inner1
  #endif
}// outer 0
}


